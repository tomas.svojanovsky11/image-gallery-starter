import { Component } from '@angular/core';

type Image = {
  id: number;
  name: string,
  likes: number,
  createdAt: string;
  imageUrl: string;

};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  image: Image = {
    id: 1,
    name: "Image 1",
    likes: 0,
    createdAt: new Date().toISOString(),
    imageUrl: "https://c.ndtvimg.com/2020-08/h5mk7js_cat-generic_625x300_28_August_20.jpg?im=Resize=(1230,900)",
  };
}
