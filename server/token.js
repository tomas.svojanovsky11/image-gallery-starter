const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
  try {
    const bearerToken = req.headers.authorization;

    if (!bearerToken) {
      return res.status(403).send("A token is required for authentication");
    }

    const token = bearerToken.split(" ")[1];
    jwt.verify(token, "randompassword45");
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }

  return next();
};

module.exports = verifyToken;
