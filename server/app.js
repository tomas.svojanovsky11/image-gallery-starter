const express = require("express");
const fs = require("fs").promises;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const cors = require('cors');

const auth = require("./token");

const router = express.Router();
const app = express()

const port = 3000;

const secretKey = "randompassword45";

app.use(express.json());
app.use(cors());

function getUser(user) {
  const copy = {...user};
  delete copy.password;

  return copy;
}

async function readFile() {
  let db = await fs.readFile(__dirname + "/db.json");
  return JSON.parse(db);
}

async function writeFile(newData) {
  const data = JSON.stringify(newData, null, 2);
  await fs.writeFile(__dirname + "/db.json", data);
}

router.get("/users", auth, async (req, res) => {
  const data = await readFile();

  const responseData = data.users.map(getUser);

  res.send(responseData);
});

router.get("/users/:id", auth, async (req, res) => {
  const data = await readFile();
  const id = Number(req.params.id);

  const found = data.users.find(user => user.id === id);

  if (!found) {
    return res.status(404).send({ message: "User not found" });
  }

  res.status(200).send(found);
});

router.get("/users/:id/images", auth, async (req, res) => {
  const data = await readFile();

  const id = Number(req.params.id);

  const images = data.images.filter(image => image.userId === id);

  res.send(images);
});

router.post("/users/:id/image", auth, async (req, res) => {
  const data = await readFile();
  const body = req.body;
  const id = Number(req.params.id);

  const newImage = {
    id: data.images.length + 1,
    userId: id,
    name: body.name,
    imageUrl: body.imageUrl,
    createdAt: new Date().toISOString(),
    likes: 0,
  };

  data.images.push(newImage);
  await writeFile(data)

  res.status(201).send(newImage);
});

router.get("/image/:id", auth, async (req, res) => {
  const data = await readFile();
  const id = Number(req.params.id);

  const image = data.images.find(image => image.id === id);

  if (!image) {
    return res.status(404).send({ message: "Image not found" });
  }

  res.status(200).send(image);
});

router.delete("/image/:id", auth, async (req, res) => {
  const data = await readFile();
  const id = Number(req.params.id);

  data.images = data.images.filter(image => image.id !== id);
  await writeFile(data);

  res.status(200).send();
});

router.patch("/image/:id", auth, async (req, res) => {
  const data = await readFile();
  const id = Number(req.params.id);
  const body = req.body;

  const image = data.images.find(image => image.id === id);

  if (!image) {
    return res.status(404).send({ message: "Image not found" });
  }

  data.images = data.images.map(image => {
    if (image.id === id) {
      return {
        ...image,
        ...body,
      };
    }

    return image;
  });
  await writeFile(data);

  res.status(200).send();
});

router.post("/user", async (req, res) => {
  const data = await readFile();
  const body = req.body;

  const user = data.users.find(user => user.email === body.email);

  if (user) {
    return res.status(409).response({ message: "User already exists" });
  }

  const salt = await bcrypt.genSalt(10);
  const password = await bcrypt.hash(body.password, salt);

  const newUser = {
    id: data.users.length + 1,
    email: body.email,
    password,
    firstName: body.firstName,
    lastName: body.lastName,
  };

  data.users.push(newUser);
  await writeFile(data);

  res.status(204).send();
});

router.post("/auth", async (req, res) => {
  const data = await readFile();
  const body = req.body;

  const user = data.users.find(user => user.email === body.email);

  if (!user) {
    return res.status(401).send({ message: "Wrong password or email" });
  }

  const validPassword = await bcrypt.compare(body.password, user.password);

  if (!validPassword) {
    return res.status(401).send({ message: "Wrong password or email" });
  }

  const token = jwt.sign(
    { user_id: user.id, email: user.email },
    secretKey,
    {
      expiresIn: "1h",
    }
  );

  res.send({
    user: getUser(user),
    accessToken: token,
  });
});

app.use("/api/v1", router);

app.listen(port, () => {
  console.log(`App is running on port: [${port}]`);
});

